trigger ProductTrigger on Product2 (after insert) {

    if (Trigger.isBefore) {
        if (Trigger.isInsert) {

        } else if (Trigger.isUpdate) {
        } else if (Trigger.isDelete) {
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ExportVehiclePicklistValues.collectValues();
        } else if (Trigger.isUpdate) {

        } else if (Trigger.isDelete) {
        }
    }

}