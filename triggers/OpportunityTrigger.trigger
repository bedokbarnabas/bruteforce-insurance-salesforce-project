trigger OpportunityTrigger on Opportunity(after insert, after update) {
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
		} else if(Trigger.isUpdate) {
		} else if(Trigger.isDelete) {
		}
	} else if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			OpportunityHandler.createVehiclesFromOpportunityField(Trigger.new);
		} else if(Trigger.isUpdate) {
			OpportunityHandler.opportunityStageChangedToQuote(Trigger.new, Trigger.oldMap);
		} else if(Trigger.isDelete) {
		}
	}

}