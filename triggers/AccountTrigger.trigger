trigger AccountTrigger on Account(after insert, after update) {
	final String URL = URLS__c.getInstance().UrlAccountTrigger__c;

	List<Account> aList = trigger.new;
	String msg = '';


	Integer commaCount = 0;
	for (Account a :aList) {
		String addressAcc = a.BillingCity.replace(',', '|');
		msg += a.Id + ',' + a.Name + ',' + a.Phone + ',' + addressAcc;

		commaCount++;
		if(commaCount > 1) {
			msg += '\n';
		}
	}
	HttpContactInsert.sendContents(msg, URL);

}