trigger QuoteTrigger on Quote(after update) {
	if(Trigger.isAfter) {
		if(Trigger.isUpdate) {
			QuoteTriggerHandler.listQuotesForPdfGenerationIfQuoteStatusApproved(Trigger.New);
			QuoteTriggerHandler.generateContractWhenOpportunityStageChangedToAccepted(Trigger.new);
		}
	}
}