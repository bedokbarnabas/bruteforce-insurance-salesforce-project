public class ExportOpportunityPicklistValues {
	public static void doIt() {
		final String pURL = '.....';
		String message = '';

		Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry s :ple) {
			message += s.getLabel() + '\n';
			System.debug('value: ' + s.getLabel());
		}
		HttpContactInsert.sendContents(message, pURL);
	}

}