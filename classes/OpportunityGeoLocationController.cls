public with sharing class OpportunityGeoLocationController {
	public String ip { get; set; }
	public String continent { get; set; }
	public String country { get; set; }
	public String country_code { get; set; }
	public String country_capital { get; set; }
	public String country_flag { get; set; }
	public String img { get; set; }
	public String latitude { get; set; }
	public String longitude { get; set; }
	public String currencyRest { get; set; }


	public OpportunityGeoLocationController(ApexPages.StandardController stdController) {
		Opportunity opp = (Opportunity) stdController.getRecord();
		opp = [SELECT Id FROM Opportunity WHERE Id = :opp.Id];

		String ip = Auth.SessionManagement.getCurrentSession().get('SourceIp');
		String requestEndpoint = 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/';
		requestEndpoint += ip;

		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setMethod('GET');
		httpRequest.setHeader('x-rapidapi-host', 'ip-geolocation-ipwhois-io.p.rapidapi.com');
		httpRequest.setHeader('x-rapidapi-key', '27fab7dc83mshfc8e9f3b477caebp193d2djsne720e313ac2c');
		httpRequest.setEndpoint(requestEndpoint);

		Http http = new Http();
		HttpResponse response = http.send(httpRequest);


		if(response.getStatusCode() == 200) {
			Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
			country= String.valueOf(results.get('country'));
			country_capital = String.valueOf(results.get('country_capital'));
			continent = String.valueOf(results.get('continent'));
			latitude = String.valueOf(results.get('latitude'));
			longitude = String.valueOf(results.get('longitude'));
			country_flag = String.valueOf(results.get('country_flag'));

		} else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error retrieving the geolocation information.');
			ApexPages.addMessage(myMsg);
			System.debug('There was an error retrieving the geolocation information.');
		}

	}

}