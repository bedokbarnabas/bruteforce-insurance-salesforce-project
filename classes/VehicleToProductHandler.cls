public class VehicleToProductHandler {
	public static void handleAfterInsert(List<Vehicle__c> vehicleList) {
		Set<String> oppIdSet = new Set<String>();
		for (Vehicle__c v :vehicleList) {
			oppIdSet.add(v.Opportunity__c);
		}

		List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE Id IN :oppIdSet];

		for(Vehicle__c veh :vehicleList) {
			Integer carYearInt = (Integer) veh.CarYear__c;
			Integer quantityInt = (Integer) veh.Quantity__c;

			FindPriceBookEntry.assignPriceBookEntry(oppList, veh.CarType__c, carYearInt, quantityInt);
		}

	}

}