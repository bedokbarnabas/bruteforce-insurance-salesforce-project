public class QuoteTriggerHandler {
	static final String QUOTE_TEMPLATE_ID = '0EH5J000000gZLO';
	public static void listQuotesForPdfGenerationIfQuoteStatusApproved(List<Quote> triggerNew) {
		Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT Id, Name FROM Quote WHERE Id IN :triggerNew AND Status = :'Approved']);
		for(Id currentQuoteId :quoteMap.keySet()) {
			QuoteTriggerHandler.generatePdf(currentQuoteId);
		}
	}
	@Future(callout = true)public static void generatePdf(Id currentQuoteId) {
		String QuoteID = currentQuoteId + '';
		String quoteURL = generateQuoteURL(currentQuoteId);
		PageReference pg = new PageReference(quoteURL);
		QuoteDocument quotedoc = new QuoteDocument();
		Blob content = pg.getContentAsPDF();

		quotedoc.Document = content;
		quotedoc.QuoteId = currentQuoteId;

		insert quotedoc;
		GenerateQuoteInPdf.sendPdfQuoteToContactInEmail(content, QuoteID);
	}

	public static String generateQuoteURL(Id quoteId) {
		String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
		quoteUrl += quoteId;
		quoteUrl += '&headerHeight=190&footerHeight=188&summlid=';
		quoteUrl += QUOTE_TEMPLATE_ID;
		quoteUrl += '#toolbar=1&navpanes=0&zoom=90';

		return quoteURL;
	}

	public static void sendPdfQuoteToContactInEmail(Blob content, ID QuoteID) {
		Quote quote = [SELECT id, Name, Contact.name, email FROM Quote WHERE id = :QuoteID LIMIT 1];
		List<String> listEmailAddress = new List<String>();
		String headerEmailBody = '';
		String strEmailBody = '';
		String strResponse = '';

		if(null != quote) {
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();

			listEmailAddress.add(quote.email);

			attachment.setFileName(quote.Name + 'Quotation.pdf');
			attachment.setBody(content);

			strResponse = quote.email;
			if(null != quote.Contact.name) {
				headerEmailBody = 'Hi ' + quote.Contact.name + ',';
			} else {
				headerEmailBody = 'Hi,';
			}  
			strEmailBody = generateStringEmailBody(headerEmailBody);

			mail.setSubject(quote.Name + ' Quotation');
			mail.setToAddresses(listEmailAddress);
			mail.setPlainTextBody(strEmailBody);
			mail.setFileAttachments(new Messaging.EmailFileAttachment [] { 
										    attachment});

			try{
				Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage [] { 
																			   mail}); }
			catch(Exception ex) {
				System.debug(ex.getMessage());
			}
		}
	}

	public static String generateStringEmailBody(String headerEmailBody) {
		String strEmailBody = '';
		strEmailBody = headerEmailBody + '\n' + '\n' + ' Below is the quotation suggested by Team regarding to Quote : ' + quote.name + '\n' + '\n';
		strEmailBody =+ '\n' + '\n' + strEmailBody + '\n' + 'Thank you.';
		return strEmailBody;

	}

	public static void generateContractWhenOpportunityStageChangedToAccepted(List<Quote> triggerNew) {
		List<Quote> quoteList = [SELECT Id, Name, Opportunity.Id, WholeQuantity__c FROM Quote WHERE Id IN :triggerNew AND Status = :'Accepted'];
		Integer numOfCars;

		for(Quote q :quoteList) {
			numOfCars = (Integer) q.WholeQuantity__c;
		}
		System.debug('*numofcars: ' + numOfcars);
		if(quoteList != null) {
			List<Contract> contractList = new List<Contract>();
			Set<Id> opportunityIdSet = getopportunityIdSet(quoteList);
			List<Opportunity> opportunityList =
		     [SELECT Id, Account.Name, Account.Id, Name, Contractual_period__c, Contract_Ends__c, 
		     Contract_start__c, CreatedById, Pricebook2Id
		     FROM Opportunity
		     WHERE id IN :opportunityIdSet AND StageName = :'Proposal/Price Quote'];

			for(Opportunity currentOpportunity :opportunityList) {
				String timeStamp = String.valueOf(datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss'));
				Contract contract = new Contract();

				contract.Name = currentOpportunity.Account.Name + '-' + timeStamp;
				contract.StartDate = currentOpportunity.Contract_start__c;
				contract.ContractTerm = (calculateMonth(currentOpportunity.Contractual_period__c));

				contract.PriceBook2Id = currentOpportunity.Pricebook2Id;

				contract.Status = 'Draft';
				contract.AccountId = currentOpportunity.Account.Id;
				contract.Description = descriptionGenerator(numOfCars);




				contractList.add(contract);


			}
			try{
				upsert contractList; }
			catch(Exception ex) {
				System.debug(ex.getMessage());

			}     
		}
	}
	public static Set<Id> getopportunityIdSet(List<Quote> quoteList) {
		Set<Id> opportunityIdSet = new Set<Id>();
		for(Quote q :quoteList) {
			opportunityIdSet.add(q.OpportunityId);
		}
		return opportunityIdSet;

	}

	public static Integer calculateMonth(String period) {
		Switch on period {
			when '3 MONTHS' {
				return 3;
			}
			when '6 MONTHS' {
				return 6;
			}
			when '12 MONTHS' {
				return 12;
			}
			when else {
				return 12;
			}
		}
	}
	public static String descriptionGenerator(Integer numberOfVehicles) {
		String carProperties = '';
		String claimAdjuster = 'estimated by Vakvarju Bt. :\n';
		List<String> cars = new List<String>();
		cars.add(claimAdjuster);
		for (Integer i = 0; i < numberOfVehicles; i ++) {
			carProperties += 'Plate number: ' + generateLicencePlate() + ' Color: ' + generateColor() + ' Condition: ' + generateConditon() + ' \n';


		}

		return claimAdjuster + carProperties;
	}
	public static String generateColor() {
		List<String> colorList = new List<String>{ 'Red', 'Blue', 'Green', 'White', 'Yellow' };
		Integer lastIndex = colorList.size() -1;
		Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));

		return colorList [randomIndex];
	}
	public static String generateConditon() {
		List<String> conditionList = new List<String>{ 'PERFECT', 'GOOD', 'AVERAGE', 'POOR' };
		Integer lastIndex = conditionList.size() -1;
		Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));

		return  conditionList [randomIndex];
	}
	public static String generateLicencePlate() {
		String plate = '';
		for(Integer i = 0; i < 3; i ++) {
			List<String> letterList = new List<String>{ 'A', 'B', 'C', 'D', 'F', 'Y', 'Z', 'R' };
			Integer lastIndex = letterList.size() -1;
			Integer randomIndex = Integer.valueOf((Math.random() * lastIndex));
			String randomLetter = letterList [randomIndex];
			plate+= randomLetter; }
		Integer randomPlateNumber = Integer.valueOf((Math.random() *1000));
		plate+= '-' + randomPlateNumber;
		return plate;
	}

}