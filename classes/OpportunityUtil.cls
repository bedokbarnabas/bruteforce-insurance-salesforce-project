public class OpportunityUtil {
	final static String FLEET = 'Flotta CASCO';

	public static void assignPriceBook(List<Opportunity> scope) {
		Pricebook2 priceBook = [SELECT Id FROM Pricebook2 WHERE Name = 'Standard Price Book'];
		for(Opportunity op :scope)
			op.Pricebook2Id = priceBook.Id;
	}

	public static void createDefaultProduct(Opportunity [] scope, String productName) {
		String queryString = FLEET + ' ' + productName;
		Product2 assignedProduct = [SELECT Id FROM Product2 WHERE Name = :queryString LIMIT 1];

		Product2 p = new Product2();
		p.Id = assignedProduct.Id;
		p.Name = queryString;

	}

	public static void parseOpportunity(List<Opportunity> scope) {
		List<OpportunityLineItem> oppLineList = new List<OpportunityLineItem>();
		String messagePhp = '';

		for (Opportunity opp :scope) {
			Id oppId = opp.Id;
			Id accId = opp.AccountId;

			messagePhp += accId + ',';

			String opportunityLineString = opp.Kocsik__c;

			if(!String.isEmpty(opportunityLineString)) {
				String[] opportunityLineListSplit = opportunityLineString.split('\\|');
				for(String line :opportunityLineListSplit) {
					System.debug('*********splitBy| ' + line);
					String[] lineSplit = line.split('\\/');
					String carMake = lineSplit.get(0);
					Integer carYear = Integer.valueOf(lineSplit [1]);
					Integer quantity = Integer.valueOf(lineSplit [2]);

				}
			}
		}

	}
}