public class HttpContactInsert {
	@future(callout = true)public static void sendContents(String msg, String pURL) {
		// Instantiate a new http object
		String urlencodedMsg = EncodingUtil.urlEncode(msg, 'UTF-8');
		String url = pURL + urlencodedMsg;
		Http h = new Http();

		// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
		HttpRequest req = new HttpRequest();
		req.setEndpoint(url);
		req.setMethod('GET');

		HttpResponse res;
		try {
			// Send the request, and return a response
			res = h.send(req);
			System.debug(res.toString());
		} catch(System.CalloutException e) {
			System.debug('********Callout error: ' + e);

		}
		If(res.getStatusCode() == 301 || res.getStatusCode() == 302) {
			req.setEndpoint(res.getHeader('Location'));

			req.setMethod('GET');
			res = new Http().send(req);
			System.debug(res.toString());
		}

		System.debug('********GET - SEND - DONE****' + res.toString());
	}



	@future(callout = true)public static void getContents(String pURL) {
		pURL   = URLS__c.getInstance().UrlHttpContactInsert__c;

		//String urlencodedMsg = EncodingUtil.urlEncode(msg, 'UTF-8');
		String url = pURL; //+ urlencodedMsg;
		Http h = new Http();

		// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
		HttpRequest req = new HttpRequest();
		req.setEndpoint(url);
		req.setMethod('GET');

		HttpResponse res;
		try {
			// Send the request, and return a response
			res = h.send(req);
			System.debug(res.toString());
			string resBody = res.getBody();

			OpportunityUpsert.parseDataFromHttpConn(resBody);
		} catch(System.CalloutException e) {
			System.debug('********Callout error: ' + e);
		}

		System.debug('********GET - - DONE****' + res.toString());
	}
}