global class ScheduledEmail implements Schedulable {
	static final String BOSS_EMAIL_ADDRESS = '***************@gmail.com';

	global void execute(SchedulableContext SC) {
		String body = '';
		List<Opportunity> expiringOpportunitiesList =
	    [SELECT Id, name, stageName, closeDate
	    FROM Opportunity
	    WHERE stageName != 'Closed Won'
	    AND closeDate = :System.today()];
		for(Opportunity o :expiringOpportunitiesList) {
			body += o.Name + ' - ' + o.stageName + ' - ' + o.closeDate + ';\n';

		}
		ScheduledEmail.sendMail(BOSS_EMAIL_ADDRESS, 'Expired!', body);
	}
	public static void sendMail(String address, String subject, String body) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String [] {
			address};
		mail.setToAddresses(toAddresses);
		mail.setSubject(subject);
		mail.setPlainTextBody(body);

		Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage [] {
																   mail });


		inspectResults(results);

	}

	private static Boolean inspectResults(Messaging.SendEmailResult [] results) {
		Boolean sendResult = true;

		for (Messaging.SendEmailResult res :results) {
			if(res.isSuccess()) {
				System.debug('Email sent successfully');
			} else {
				sendResult = false;
				System.debug('The following errors occurred: ' + res.getErrors());
			}
		}

		return sendResult;
	}

}