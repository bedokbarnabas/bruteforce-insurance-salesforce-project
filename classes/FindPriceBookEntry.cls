public class FindPriceBookEntry {
	final static String DEFAULT_INSURANCE_PRODUCT = 'Flotta CASCO ';
	final static String STANDARD_PRICEBOOK = 'Standard Price Book';

	public static void assignPriceBookEntry(List<Opportunity> opportunityList, String carType, Integer carYear, Integer quantityFromOpportunity) {
		Map<string, PricebookEntry> priceBooksMap = new Map<String, PricebookEntry>();
		Pricebook2 selectedPriceBook = getPriceBook(STANDARD_PRICEBOOK);

		List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();

		String productName = DEFAULT_INSURANCE_PRODUCT + carType;
		Product2 assignedProductToVehicle = getAssignedProduct(productName);

		String productPriceBookEntryName;
		Map<Id, String> opportunityIdProductNameMap = new Map<Id, String>();

		Map<String, Vehicle__c> vehiclePriceBookEntryMap = new Map<String, Vehicle__c>();

		Set<Id> opportunityIdSet = getOpportunityIdSet(opportunityList);

		Map<Id, Vehicle__c> vehicleMap = getVehicleMap(opportunityIdSet);


		for (Opportunity op :opportunityList) {
		
			for (Vehicle__c v :vehicleMap.values()) {
				v.PriceFactorByYear__c = VintageCalculator.getPricefactorFromCache(Integer.valueOf(v.CarYear__c));

				productPriceBookEntryName = DEFAULT_INSURANCE_PRODUCT + CarType;
				priceBooksMap.put(productPriceBookEntryName, null);
				opportunityIdProductNameMap.put(op.Id, productPriceBookEntryName);

				vehiclePriceBookEntryMap.put(productPriceBookEntryName, v);
			}
		}
		List<PricebookEntry> priceBookEntryList = getPriceBookEntries(selectedPriceBook, priceBooksMap);

		for (PricebookEntry currentPriceBookEntry :priceBookEntryList) {
			priceBooksMap.put(currentPriceBookEntry.product2.Name, currentPriceBookEntry);
		}
		for (Opportunity op :opportunityList) {
			OpportunityLineItem ol = new OpportunityLineItem();
			ol.opportunityId = op.Id;
			ol.unitprice = priceBooksMap.get(opportunityIdProductNameMap.get(op.Id)).unitprice;
			ol.quantity = quantityFromOpportunity;
			ol.pricebookentryid = priceBooksMap.get(opportunityIdProductNameMap.get(op.Id)).id;

			opportunityLineItemList.add(ol);
		}
		insert opportunityLineItemList;

	}
	public static PriceBook2 getPriceBook(String priceBookName) {
		return [SELECT id FROM pricebook2 WHERE name = :priceBookName LIMIT 1]; }

	public static Product2 getAssignedProduct(String productName) {
		return [SELECT Id, Name FROM Product2 WHERE Name = :productName LIMIT 1];
	}

	public static List<PricebookEntry> getPriceBookEntries(Pricebook2 selectedPriceBook, Map<String, PricebookEntry> priceBooksMap) {
		return [SELECT id, unitprice, product2id, product2.Name FROM PricebookEntry WHERE product2.Name IN :priceBooksMap.keyset() AND pricebook2id = :selectedPriceBook.id];
	}

	public static Set<Id> getOpportunityIdSet(List<Opportunity> opportunityList) {
		Set<Id> opportunityIdSet = new Set<Id>();
		for(Opportunity o :opportunityList) {
			opportunityIdSet.add(o.Id);
		}
		return opportunityIdSet;


	}
	public static Map<Id, Vehicle__c> getVehicleMap(Set<Id> opportunityIdSet) {
		return new Map<Id, Vehicle__c>([SELECT Id, Opportunity__r.Id, CarType__c, PriceFactorByYear__c, CarYear__c, Quantity__c
									    FROM Vehicle__c
									    WHERE Opportunity__r.Id
									    IN:opportunityIdSet]); }
}