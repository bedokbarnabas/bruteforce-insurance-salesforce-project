public class ExportVehiclePicklistValues {
	public static void collectValues() {
		final String pURL = '....';
		String message = '';

		List<Product2> productList = [SELECT Name FROM Product2];

		for(Product2 p :productList) {
			p.Name = p.Name.removeStartIgnoreCase('Flotta Casco ');
			message += p.Name + '\n';
		}

		HttpContactInsert.sendContents(message, pURL);
	}

}