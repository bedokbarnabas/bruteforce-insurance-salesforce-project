public class VintageCalculator {
	private static final Integer ROUND_UP_TO_DECIMAL_PLACES = 4;

	private static Map<Decimal, Decimal> priceFactorMap = new Map<Decimal, Decimal>();
	private final static Decimal YEAR_BASE;
	private final static Decimal YEAR_PIVOT;
	private final static Decimal YEAR_PRESENT;

	private final static Double FACTOR_YEAR_BASE;
	private final static Double FACTOR_YEAR_PIVOT;
	private final static Double FACTOR_YEAR_PRESENT;

	static{
		priceFactorMap.clear();

		VehicleYear__c vehYear = VehicleYear__c.getInstance();
		YEAR_BASE = vehYear.BaseYear__c;
		YEAR_PIVOT= vehYear.PivotYear__c;
		YEAR_PRESENT= vehYear.PresentYear__c;

		PriceFactor__c pFactor = PriceFactor__c.getInstance();
		FACTOR_YEAR_BASE = pFactor.FactorBaseYear__c;
		FACTOR_YEAR_PIVOT= pFactor.FactorPivotYear__c;
		FACTOR_YEAR_PRESENT= pFactor.FactorPresentYear__c;


		calculateFactorFromVintage();
	}


	public static void calculateFactorFromVintage() {
		Decimal yearsPassedFromPresentToPivot = math.abs(YEAR_PRESENT - YEAR_PIVOT);
		Decimal yearsPassedFromPivotToBase = math.abs(YEAR_PIVOT - YEAR_BASE);
		Double decreaseFactorPresentPivot = math.abs(FACTOR_YEAR_PRESENT - FACTOR_YEAR_PIVOT) / yearsPassedFromPresentToPivot;
		Double increaseFactorPivotBase = math.abs(FACTOR_YEAR_BASE - FACTOR_YEAR_PIVOT) / yearsPassedFromPivotToBase;

		Double factorCurrent = FACTOR_YEAR_PRESENT;

		for(Decimal year = YEAR_PRESENT; year >= YEAR_PIVOT; year --) {
			Decimal valueScaled = ((Decimal) factorCurrent).setScale(ROUND_UP_TO_DECIMAL_PLACES);
			priceFactorMap.put(year, valueScaled);
			if(year != YEAR_PIVOT) { 
				factorCurrent -= decreaseFactorPresentPivot; }
		}



		for(Decimal year = YEAR_PIVOT - 1; year >= YEAR_BASE; year --) {
			factorCurrent += increaseFactorPivotBase;
			Decimal valueScaled = ((Decimal) factorCurrent).setScale(ROUND_UP_TO_DECIMAL_PLACES);
			priceFactorMap.put(year, valueScaled);
		}

		for(Decimal y :priceFactorMap.keySet()) {
			System.debug('year: ' + y + ' | factor: ' + priceFactorMap.get(y));

		}


	}   



	public static Decimal getPricefactorFromCache(Integer year) {
		System.debug('********PRICEMAPPPP ' + priceFactorMap);
		return  priceFactorMap.get(year);
	}



}