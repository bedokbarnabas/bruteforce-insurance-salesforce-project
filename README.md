# BruteForce Insurance Salesforce project

During the Salesforce project work, we developed an app in Salesforce that supports the selling comprehensive 
insurance product (named Casco) of a car fleet and automates certain business processes.

The product is chosen that matches most the incoming parameters, an automated quotation is created and sent.
Apex triggers, classes, standard and custom Salesforce objects have been used.

Main features:
- Accepting web-to-lead, web-to-case form sources from outer website to generate, modify Opportunities.
- Car's brand, year of production, quantity  gathered in a 'Vehicle' custom Salesforce object.
- Setting Opportunity to 'Proposal/Price Quote'  stage creates a quote.
- Setting Quote to approved state generates .pdf Quotes, automatically sent to the Contact's email address. 
- Setting Quote to accepted state creates a draft Contract.
- Customizable pricebooks.
- Unhandled Opportunities automatically sent to responsible leader's address every 24h.